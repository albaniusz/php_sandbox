<?php
$triggerName = "trigger";
$triggerValue = "true";
$fileName = "mBankData1216.txt";
$fileStorage = "../resources/";
$fileLocation = $fileStorage . $fileName;
$logFileName = "mBankLog.txt";
$logFileLocation = $fileStorage . $logFileName;
?>

<form method="post" target="">
    <input type="hidden" name="<?= $triggerName ?>" value="true" />
    <input type="submit" name="Parsuj"/>
</form>

<?php
if (array_key_exists($triggerName, $_POST) && $_POST[$triggerName] == $triggerValue || true == true) {
    dprint("triggered");
    dprint("");

    $result = array();

    $logger = new BasicLogger($logFileLocation);

    $handle = fopen($fileLocation, "r");
    if ($handle) {
        $lastLine = "";
        $curentLine = "";

        while (($line = fgets($handle)) !== false) {
            $curentLine = trim($line);

            if ($curentLine != null && $curentLine != "") {
                $logger->log("curentLine: " . $curentLine);

                if (preg_match("#^-*([0-9\ ]*,[0-9]{2})(.*)$#i", $curentLine)) {
                    $logger->log("znaleziono linie: " . $curentLine);

                    $amountArray = preg_split("/PLN/", $curentLine);
                    if (key_exists(0, $amountArray)) {
                        $amount = str_replace(" ", "", $amountArray[0]);
                        $amount = trim($amount);

                        // jezeli kwota jest ujemna
                        // ustawiam znacznik na ujecie wartosci
                        $isCharge = $amount[0] == "-" ? true : false;
                        if ($isCharge) {
                            $logger->log("kwota ujemna");
                        }

                        $amountArray = preg_split("/,/", $amount);
                        $amountInteger = intval($amountArray[0]) * 100;
                        $amountFraction = intval($amountArray[1]);
                        if ($isCharge) {
                            $amountFraction *= -1;
                        }

                        $amount = $amountInteger + $amountFraction;
                        $logger->log("amount: " . $amount);

                        // szukaj w kolekcji wynikow po kluczu
                        $keyName = parseKeyName($lastLine);
                        if (!key_exists($keyName, $result)) {
                            $result[$keyName] = $amount;
                        } else {
                            $result[$keyName] += $amount;
                        }
                    } else {
                        throw new Exception("Nie znaleziono kwoty");
                    }
                }
            }
            $lastLine = $curentLine;
        }

        fclose($handle);

        printResult($result);
    } else {
        dprint("ERROR");
    }
}

function dprint($input, $noBreakLine = false) {
    echo nl2br($input . ($noBreakLine ? "" : "<br/>"));
}

function parseKeyName($keyName) {
    $search = array(',', ' ', 'Ę', 'ę', 'Ó', 'ó', 'Ą', 'ą', 'Ś', 'ś', 'Ł', 'ł', 'Ż', 'ż', 'Ź', 'ź', 'Ń', 'ń', 'Ć', 'ć');
    $replace = array('', '_', 'E', 'e', 'O', 'o', 'A', 'a', 'S', 's', 'L', 'l', 'Z', 'z', 'Z', 'z', 'N', 'n', 'C', 'c');

    return strtolower(str_replace($search, $replace, $keyName));
}

function printResult($result) {
    dprint("RESULT: ");
    foreach ($result as $key => $value) {
        $isNegative = false;
        if ($value < 0) {
            $value *= -1;
            $isNegative = true;
        }

        $colorL = "";
        $colorR = "";

        if ($isNegative) {
            $colorL = "<font color=\"red\">";
            $colorR = "</font>";
        }

        dprint($colorL . $key . " : " . money_format('%.2n', floatval($value) / 100) . $colorR);
    }
}

class BasicLogger {

    private $fileHandler;

    public function __construct($logFileLocation) {
        // open file 
        $this->fileHandler = fopen($logFileLocation, "a");
    }

    public function log($logEntry) {
        // write string 
        $logEntry = "[" . date("Y/m/d h:i:s", time()) . "] " . $logEntry;
        fwrite($this->fileHandler, $logEntry . "\n");
    }

    public function __destruct() {
        // close file 
        fclose($this->fileHandler);
    }

}
